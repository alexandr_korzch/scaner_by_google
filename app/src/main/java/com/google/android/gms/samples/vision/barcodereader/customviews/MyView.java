package com.google.android.gms.samples.vision.barcodereader.customviews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.View;

import com.google.android.gms.samples.vision.barcodereader.ui.camera.CameraSourcePreview;

/**
 * Created by DWork on 3/29/2016.
 */
public class MyView extends View {


    private CameraSourcePreview cameraSourcePreview;

    public MyView(Context context) {
        super(context);
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = canvas.getWidth();
        int height = canvas.getHeight();

        int squareWidth = 0;
        int squareHeight = 0;

        if (getResources().getConfiguration().orientation == 1) {
            squareWidth = (int) (width - (float) width * 0.3f);
            squareHeight = squareWidth;
        }else if(getResources().getConfiguration().orientation == 2){
            squareHeight = (int) (height - (float) height * 0.3f);
            squareWidth = squareHeight;
        }

        int x1 = (width - squareWidth) / 2;
        int x2 = x1 + squareWidth;
        int y1 = (height - squareHeight) / 2;
        int y2 = y1 + squareHeight;

//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.icon);
        Bitmap b = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(b);

        b.eraseColor(Color.WHITE);

        Paint eraser = new Paint();
        eraser.setColor(0xFFFFFFFF);
        eraser.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        c.drawRect(x1, y1, x2, y2, eraser);

        canvas.drawBitmap(b, 0, 0, null);

//        Log.d("tag", "cameraSourcePreview.getmSurfaceView() - "+cameraSourcePreview.getmSurfaceView());

//        cameraSourcePreview.getmSurfaceView().draw(canvas);

//        invalidate();


    }

    public void setCameraSourcePreview(CameraSourcePreview cameraSourcePreview) {
        this.cameraSourcePreview = cameraSourcePreview;
    }
}